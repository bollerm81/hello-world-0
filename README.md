# Introduction
This is a readme file. These files are always the first thing you should read,
as they will describe the task you need to complete. They will always be written
in [Markdown](https://daringfireball.net/projects/markdown/syntax), which uses
symbols to indicate greater meaning in plaintext. However, if you view this file
online, the repository will render the Markdown into a nice document. Still, you
can always choose to simply open this file on your computer after you clone this
repository as well. Enough with the introduction; let's move on to writing your
first program!

# Task
For your first task, you will be writing the classical first program you write
in any language: the "hello world" program. This program simply prints out
"hello, world!" to the console and exits.

Type the following into hello-world.py, which is included for you in this
repository.
```
print('hello, world!')
```

Save the file. We will now run the program. To do this, we use a special program
called the Python interpreter, which reads our code line-by-line and performs
the proper actions on the computer.

You can invoke the Python interpreter in several ways, but the simplest is to
click the **run** button (it looks like a little green play button) at the top
of the screen.

It might appear that nothing has happened, but glance down to the console window
at the bottom of your screen. If you can't see the output, 
scroll to the bottom of the output screen. Sure enough, the 
text outputed will be ** hello, world!**.
